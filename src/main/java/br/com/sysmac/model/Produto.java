package br.com.sysmac.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "PRODUTO")
public class Produto  {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private Long codigoBarras;
    private String descricao;
    private String ref;
    private float valorVenda;

    @ManyToMany
    private List<Fornecedor> fornecedorList;

    private ProdutoValores produtoValores;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCodigoBarras() {
        return codigoBarras;
    }

    public void setCodigoBarras(Long codigoBarras) {
        this.codigoBarras = codigoBarras;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public float getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(float valorVenda) {
        this.valorVenda = valorVenda;
    }

    public List<Fornecedor> getFornecedorList() {
        return fornecedorList;
    }

    public void setFornecedorList(List<Fornecedor> fornecedorList) {
        this.fornecedorList = fornecedorList;
    }

    public ProdutoValores getProdutoValores() {
        return produtoValores;
    }

    public void setProdutoValores(ProdutoValores produtoValores) {
        this.produtoValores = produtoValores;
    }
}
