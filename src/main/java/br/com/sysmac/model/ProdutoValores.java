package br.com.sysmac.model;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProdutoValores {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProdutoValores.class);

    private float valorCompra;
    private float valorVenda;
    private float porcentagem;

    public  float valorVendaCusto() {
        if (this.valorCompra < this.valorVenda) {
            return valorVenda + porcentagem;
        } else {
            return valorVenda = valorCompra + 10;
        }
    }

    public float getValorCompra() {
        return valorCompra;
    }

    public void setValorCompra(float valorCompra) {
        this.valorCompra = valorCompra;
    }

    public float getValorVenda() {
        return valorVenda;
    }

    public void setValorVenda(float valorVenda) {
        this.valorVenda = valorVenda;
    }

    public static Logger getLOGGER() {
        return LOGGER;
    }

    public float getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(float porcentagem) {
        this.porcentagem = porcentagem;
    }
}
