package br.com.sysmac;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SysmacApiDevApplication {

    public static void main(String[] args) {
        SpringApplication.run(SysmacApiDevApplication.class, args);
    }

}
